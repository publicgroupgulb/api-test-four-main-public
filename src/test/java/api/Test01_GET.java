package com.restassured.api;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import io.restassured.response.Response;


import static org.testng.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.testng.Assert;
import io.restassured.http.ContentType;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import static org.hamcrest.MatcherAssert.*;
import io.restassured.path.json.JsonPath;

import io.restassured.RestAssured.*;
import io.restassured.matcher.RestAssuredMatchers.*;
import org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;


import com.restassured.api.Requests;

import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
import io.restassured.RestAssured;
class Base{
	@BeforeAll
	public static void init() {
		RestAssured.baseURI = "https://swapi.dev/api/";
//		RestAssured.port=8080; //commented as our API doesnot use port
		//RestAssured.basePath = "sample/api/product";
	}
}




public class Test01_GET {




	@Test(priority = 1)
	void test_001() {

		//All Api should return 200

		Requests a = new Requests();
        a.apishouldwork("/people/");
		a.apishouldwork("/planets/");
		a.apishouldwork("/starships/");

	}


	@Test
	void test_002() {

		//First person of people's name should be "luke Skywalker"

		Requests a = new Requests();
		Response responsetwo = a.chooseapiandxthelementasresponse("/people/", "1");
		System.out.println(responsetwo.asString());
		String myresponseget = responsetwo.asString();
		myresponseget.contains("Luke Skywalker");
		System.out.println(responsetwo.getBody().asString());
		


}

	@Test 
	void test_003(){
		//We assert the properities of APIs easily via equalality method

		Requests a = new Requests();
		a.apiselementpropertyvalidation("/people/", "1","name","Luke Skywalker");
		a.apiselementpropertyvalidation("/people/", "1","mass","77");
		a.apiselementpropertyvalidation("/people/", "1","height","172");
		a.apiselementpropertyvalidation("/starships/", "9","name","Death Star");

	
}

	@Test
	void test_004(){

		//Planet's name in the resuls should have these items i.e "Tatooine","Alderaan","Yavin IV","Hoth","Dagobah","Bespin","Endor","Naboo","Coruscant","Kamino"

		given()
		.get("https://swapi.dev/api/planets/")
		.then()
		.body("results.name",  hasItems("Tatooine","Alderaan","Yavin IV","Hoth","Dagobah","Bespin","Endor","Naboo","Coruscant","Kamino"));
	}

	@Test 
	void test_005(){	
		//Assert that The population of the planet with a gravity of "1.5 (surface), 1 standard (Cloud City)"" is equal to 6000000
		when().
       	get("https://swapi.dev/api/planets/").
		then().
       body("results.findAll { it.gravity == '1.5 (surface), 1 standard (Cloud City)'}.population", hasItems("6000000", "6000000"));
 

	}

	@Test 
	void test_006(){
		//Assert that The number of all planets is greater than 59
		when().
       	get("https://swapi.dev/api/planets/").
		then().
       	body("results.name.collect { it.length() }.sum()", greaterThan(59));
		
	}


	@Test 
	//Api duration should be less than 2000 Milliseconds
	void test_007(){
		Requests a = new Requests();
		a.checkapiperformance("/people/",20000L);
 

	}


	@Test 
	void test_008(){
		//Dummy example. No assertion!
	byte[] byteArray = get("https://swapi.dev/api/people/").asByteArray();
	String json = get("https://swapi.dev/api/people/").asString();

	}





}
