# rest-assured-example
This repository contains some examples with rest-assured framework

To understand what rest-assured is go to this ref http://rest-assured.io and https://github.com/rest-assured/rest-assured/wiki/Usage#getting-response-data


I prefered this API: https://swapi.dev/

To run tests you need JDK 1.7+
To build it you'll need maven3

